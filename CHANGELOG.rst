=============
kafka-formula
=============

0.2.1 (2017-11-29)

- Start and enable Kafka service instead of just enabling it

0.2.0 (2017-05-22)

- Modified pillar/defaults structure
- Load source digest from official digest files (after some ``grep``, ``sed`` and so on...)
- Added .SLS file with macros

0.1.0 (2017-02-15)

- First release

